import unittest
from unittest.mock import Mock

from Commands.CheckFuelCommand import CheckFuel
from Commands.CommandException import CommandException


class TestsCheckFuel(unittest.TestCase):

    def test_checkFuel(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        CheckFuel(with_fuel=_mock_adapter).execute()
        # check get fuel method
        _mock_adapter.get_fuel.assert_called_once()
        # check get burn rate method
        _mock_adapter.get_burn_rate.assert_called_once()

    def test_checkFuel_negative(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_fuel': Mock(return_value=3),
                 'get_burn_rate': Mock(return_value=4)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=CommandException) as context:
            CheckFuel(with_fuel=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Not enough fuel to carry out the command.')

    def test_checkFuel_without_fuel(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_fuel': Mock(side_effect=KeyError('Fuel')),
                 'get_burn_rate': Mock(return_value=4)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            CheckFuel(with_fuel=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object without fuel. Details: Fuel')

    def test_checkFuel_without_burn_rate(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_fuel': Mock(return_value=3),
                 'get_burn_rate': Mock(side_effect=KeyError('Burn rate'))}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            CheckFuel(with_fuel=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object without fuel. Details: Burn rate')

