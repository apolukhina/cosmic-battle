import unittest
from unittest.mock import Mock
from Commands.RotateCommand import Rotate


class TestsRotate(unittest.TestCase):

    def test_rotate(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_direction': Mock(return_value=12),
                 'get_angular_velocity': Mock(return_value=5),
                 'get_directions_number': Mock(return_value=36),
                 'set_direction': Mock(side_effect=lambda direction: direction)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        Rotate(rotatable=_mock_adapter).execute()
        # check get directions method
        _mock_adapter.get_direction.assert_called_once()
        # check get angular velocity method
        _mock_adapter.get_angular_velocity.assert_called_once()
        # check get directions number method
        _mock_adapter.get_directions_number.assert_called_once()
        # Validate data
        _mock_adapter.set_direction.assert_called_with(direction=17)

    def test_rotate_without_direction(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_direction': Mock(side_effect=KeyError('Direction')),
                 'get_angular_velocity': Mock(return_value=5),
                 'get_directions_number': Mock(return_value=36),
                 'set_direction': Mock(side_effect=lambda direction: direction)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            Rotate(rotatable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object cannot rotate. Details: Direction')

    def test_rotate_without_angular_velocity(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_direction':  Mock(return_value=12),
                 'get_angular_velocity': Mock(side_effect=KeyError('Angular velocity')),
                 'get_directions_number': Mock(return_value=36),
                 'set_direction': Mock(side_effect=lambda direction: direction)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            Rotate(rotatable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object cannot rotate. Details: Angular velocity')

    def test_rotate_without_directions_number(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_direction':  Mock(return_value=12),
                 'get_angular_velocity': Mock(return_value=5),
                 'get_directions_number': Mock(side_effect=KeyError('Directions number')),
                 'set_direction': Mock(side_effect=lambda direction: direction)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            Rotate(rotatable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object cannot rotate. Details: Directions number')

    def test_rotate_without_setDirection(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_direction':  Mock(return_value=12),
                 'get_angular_velocity': Mock(return_value=5),
                 'get_directions_number': Mock(return_value=36),
                 'set_direction': Mock(side_effect=AttributeError('Object has no attributes set_direction'))}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=AttributeError) as context:
            Rotate(rotatable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Object attributes error: Object has no attributes set_direction')
