import unittest
from unittest.mock import Mock
from Models.Position import Position
from Commands.MoveCommand import Move


class TestsMove(unittest.TestCase):

    def test_move(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_position': Mock(return_value=Position(x=12, y=5)),
                 'get_velocity.return_value': Position(x=-7, y=3),
                 'set_position': Mock(side_effect=lambda position: position)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        Move(movable=_mock_adapter).execute()
        # check get positions method
        _mock_adapter.get_position.assert_called_once()
        # check get velocity method
        _mock_adapter.get_velocity.assert_called_once()
        # Validate data
        _mock_adapter.set_position.assert_called_with(position=Position(5, 8))

    def test_move_without_position(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_position': Mock(side_effect=KeyError('Position')),
                 'get_velocity.return_value': Position(x=-7, y=3),
                 'set_position': Mock(side_effect=lambda position: position)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            Move(movable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object cannot move. Details: Position')

    def test_move_without_velocity(self):
        _mock_adapter = Mock()
        attrs = {'get_position': Mock(return_value=Position(x=12, y=5)),
                 'get_velocity': Mock(side_effect=KeyError('Velocity')),
                 'set_position': Mock(side_effect=lambda position: position)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            Move(movable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object cannot move. Details: Velocity')

    def test_move_without_setPosition(self):
        _mock_adapter = Mock()
        attrs = {'get_position': Mock(return_value=Position(x=12, y=5)),
                 'get_velocity': Mock(return_value=Position(x=-7, y=3)),
                 'set_position': Mock(side_effect=AttributeError('Object has no attributes set_position'))}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=AttributeError) as context:
            Move(movable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Object attributes error: Object has no attributes set_position')


if __name__ == '__main__':
    unittest.main()
