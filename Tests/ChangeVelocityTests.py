import unittest
from unittest.mock import Mock

from Commands.ChangeVelocityCommand import ChangeVelocity
from Models.Position import Position


class TestsChangeVelocity(unittest.TestCase):

    def test_changeVelocity(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_velocity': Mock(return_value=Position(x=4, y=-3)),
                 'get_direction': Mock(return_value=5),
                 'get_directions_number': Mock(return_value=36),
                 'set_velocity': Mock(side_effect=lambda velocity: velocity)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        ChangeVelocity(velocity_changeable=_mock_adapter).execute()
        _mock_adapter.get_velocity.assert_called_once()
        _mock_adapter.get_direction.assert_called_once()
        _mock_adapter.get_directions_number.assert_called_once()
        # validate data
        _mock_adapter.set_velocity.assert_called_with(velocity=Position(x=5, y=-1))

    def test_changeVelocity_without_velocity(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_velocity': Mock(side_effect=KeyError('Velocity')),
                 'get_direction': Mock(return_value=5),
                 'get_directions_number': Mock(return_value=36),
                 'set_velocity': Mock(side_effect=lambda velocity: velocity)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            ChangeVelocity(velocity_changeable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object is not velocity changeable. Details: Velocity')

    def test_changeVelocity_without_direction(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_velocity': Mock(return_value=Position(x=4, y=-3)),
                 'get_direction': Mock(side_effect=KeyError('Direction')),
                 'get_directions_number': Mock(return_value=36),
                 'set_velocity': Mock(side_effect=lambda velocity: velocity)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            ChangeVelocity(velocity_changeable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object is not velocity changeable. Details: Direction')

    def test_changeVelocity_without_directions_number(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_velocity': Mock(return_value=Position(x=4, y=-3)),
                 'get_direction': Mock(return_value=5),
                 'get_directions_number': Mock(side_effect=KeyError('Directions number')),
                 'set_velocity': Mock(side_effect=lambda velocity: velocity)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            ChangeVelocity(velocity_changeable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0],
                         'Such object is not velocity changeable. Details: Directions number')

    def test_changeVelocity_without_set_velocity(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_velocity': Mock(return_value=Position(x=4, y=-3)),
                 'get_direction': Mock(return_value=5),
                 'get_directions_number': Mock(return_value=36),
                 'set_velocity': Mock(side_effect=AttributeError('set_velocity'))}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=AttributeError) as context:
            ChangeVelocity(velocity_changeable=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Object attributes error: set_velocity')
