import unittest
from unittest.mock import Mock

from Commands.BurnFuelCommand import BurnFuel


class TestsBurnFuel(unittest.TestCase):

    def test_burnFuel(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4),
                 'set_fuel': Mock(side_effect=lambda fuel: fuel)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        BurnFuel(with_fuel=_mock_adapter).execute()
        # check get fuel method
        _mock_adapter.get_fuel.assert_called_once()
        # check get burn rate method
        _mock_adapter.get_burn_rate.assert_called_once()
        # validate data
        _mock_adapter.set_fuel.assert_called_with(fuel=16)

    def test_burnFuel_without_fuel(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_fuel': Mock(side_effect=KeyError('Fuel')),
                 'get_burn_rate': Mock(return_value=4),
                 'set_fuel': Mock(side_effect=lambda fuel: fuel)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            BurnFuel(with_fuel=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object without fuel. Details: Fuel')

    def test_burnFuel_without_burn_rate(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_fuel': Mock(return_value=3),
                 'get_burn_rate': Mock(side_effect=KeyError('Burn rate')),
                 'set_fuel': Mock(side_effect=lambda fuel: fuel)}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=KeyError) as context:
            BurnFuel(with_fuel=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Such object without fuel. Details: Burn rate')

    def test_burnFuel_without_set_fuel(self):
        # Prepared data
        _mock_adapter = Mock()
        attrs = {'get_fuel': Mock(return_value=5),
                 'get_burn_rate': Mock(return_value=4),
                 'set_fuel': Mock(side_effect=AttributeError('set_fuel'))}
        _mock_adapter.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=AttributeError) as context:
            BurnFuel(with_fuel=_mock_adapter).execute()
        self.assertEqual(context.exception.args[0], 'Object attributes error: set_fuel')
