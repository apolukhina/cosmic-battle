import unittest
from unittest.mock import Mock

from Models.Position import Position

from Commands.MoveCommand import MoveWithFuel
from Commands.CheckFuelCommand import CheckFuel
from Commands.MoveCommand import Move
from Commands.BurnFuelCommand import BurnFuel
from Commands.CommandException import CommandException


class TestsMoveWithFuel(unittest.TestCase):

    def test_moveWithFuel(self):
        # Check fuel
        _mock_check_fuel = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4)}
        _mock_check_fuel.configure_mock(**attrs)
        # Move
        _mock_move = Mock()
        attrs = {'get_position': Mock(return_value=Position(x=12, y=5)),
                 'get_velocity.return_value': Position(x=-7, y=3),
                 'set_position': Mock(side_effect=lambda position: position)}
        _mock_move.configure_mock(**attrs)
        # Burn fuel
        _mock_burn_fuel = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4),
                 'set_fuel': Mock(side_effect=lambda fuel: fuel)}
        _mock_burn_fuel.configure_mock(**attrs)
        # Main steps
        MoveWithFuel(check_fuel=CheckFuel(with_fuel=_mock_check_fuel),
                     move=Move(movable=_mock_move),
                     burn_fuel=BurnFuel(with_fuel=_mock_burn_fuel)).execute()
        # Check fuel
        _mock_check_fuel.get_fuel.assert_called_once()
        _mock_check_fuel.get_burn_rate.assert_called_once()
        # Move
        _mock_move.get_position.assert_called_once()
        _mock_move.get_velocity.assert_called_once()
        _mock_move.set_position.assert_called_with(position=Position(5, 8))
        # Burn fuel
        _mock_burn_fuel.get_fuel.assert_called_once()
        _mock_burn_fuel.get_burn_rate.assert_called_once()
        _mock_burn_fuel.set_fuel.assert_called_with(fuel=16)

    def test_moveWithFuel_checkFuel_error(self):
        # Check fuel
        _mock_check_fuel = Mock()
        attrs = {'get_fuel': Mock(side_effect=KeyError('Fuel')),
                 'get_burn_rate': Mock(return_value=4)}
        _mock_check_fuel.configure_mock(**attrs)
        # Move
        _mock_move = Mock()
        attrs = {'get_position': Mock(return_value=Position(x=12, y=5)),
                 'get_velocity.return_value': Position(x=-7, y=3),
                 'set_position': Mock(side_effect=lambda position: position)}
        _mock_move.configure_mock(**attrs)
        # Burn fuel
        _mock_burn_fuel = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4),
                 'set_fuel': Mock(side_effect=lambda fuel: fuel)}
        _mock_burn_fuel.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=CommandException) as context:
            MoveWithFuel(check_fuel=CheckFuel(with_fuel=_mock_check_fuel),
                         move=Move(movable=_mock_move),
                         burn_fuel=BurnFuel(with_fuel=_mock_burn_fuel)).execute()
        self.assertEqual(context.exception.args[0],
                         'Something error while executing commands. Details: Such object without fuel. Details: Fuel')
        # Check fuel
        _mock_check_fuel.get_fuel.assert_called_once()
        _mock_check_fuel.get_burn_rate.assert_not_called()
        # Move
        _mock_move.get_position.assert_not_called()
        _mock_move.get_velocity.assert_not_called()
        _mock_move.set_position.assert_not_called()
        # Burn fuel
        _mock_burn_fuel.get_fuel.assert_not_called()
        _mock_burn_fuel.get_burn_rate.assert_not_called()
        _mock_burn_fuel.set_fuel.assert_not_called()

    def test_moveWithFuel_move_error(self):
        # Check fuel
        _mock_check_fuel = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4)}
        _mock_check_fuel.configure_mock(**attrs)
        # Move
        _mock_move = Mock()
        attrs = {'get_position': Mock(side_effect=KeyError('Position')),
                 'get_velocity.return_value': Position(x=-7, y=3),
                 'set_position': Mock(side_effect=lambda position: position)}
        _mock_move.configure_mock(**attrs)
        # Burn fuel
        _mock_burn_fuel = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4),
                 'set_fuel': Mock(side_effect=lambda fuel: fuel)}
        _mock_burn_fuel.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=CommandException) as context:
            MoveWithFuel(check_fuel=CheckFuel(with_fuel=_mock_check_fuel),
                         move=Move(movable=_mock_move),
                         burn_fuel=BurnFuel(with_fuel=_mock_burn_fuel)).execute()
        self.assertEqual(context.exception.args[0],
                         'Something error while executing commands. '
                         'Details: Such object cannot move. Details: Position')
        # Check fuel
        _mock_check_fuel.get_fuel.assert_called_once()
        _mock_check_fuel.get_burn_rate.assert_called_once()
        # Move
        _mock_move.get_position.assert_called_once()
        _mock_move.get_velocity.assert_not_called()
        _mock_move.set_position.assert_not_called()
        # Burn fuel
        _mock_burn_fuel.get_fuel.assert_not_called()
        _mock_burn_fuel.get_burn_rate.assert_not_called()
        _mock_burn_fuel.set_fuel.assert_not_called()

    def test_moveWithFuel_burnFuel_error(self):
        # Check fuel
        _mock_check_fuel = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4)}
        _mock_check_fuel.configure_mock(**attrs)
        # Move
        _mock_move = Mock()
        attrs = {'get_position': Mock(return_value=Position(x=12, y=5)),
                 'get_velocity.return_value': Position(x=-7, y=3),
                 'set_position': Mock(side_effect=lambda position: position)}
        _mock_move.configure_mock(**attrs)
        # Burn fuel
        _mock_burn_fuel = Mock()
        attrs = {'get_fuel': Mock(return_value=20),
                 'get_burn_rate': Mock(return_value=4),
                 'set_fuel': Mock(side_effect=AttributeError('set_fuel'))}
        _mock_burn_fuel.configure_mock(**attrs)
        # Main steps
        with self.assertRaises(expected_exception=CommandException) as context:
            MoveWithFuel(check_fuel=CheckFuel(with_fuel=_mock_check_fuel),
                         move=Move(movable=_mock_move),
                         burn_fuel=BurnFuel(with_fuel=_mock_burn_fuel)).execute()
        self.assertEqual(context.exception.args[0],
                         'Something error while executing commands. Details: Object attributes error: set_fuel')
        # Check fuel
        _mock_check_fuel.get_fuel.assert_called_once()
        _mock_check_fuel.get_burn_rate.assert_called_once()
        # Move
        _mock_move.get_position.assert_called_once()
        _mock_move.get_velocity.assert_called_once()
        _mock_move.set_position.assert_called_with(position=Position(5, 8))
        # Burn fuel
        _mock_burn_fuel.get_fuel.assert_called_once()
        _mock_burn_fuel.get_burn_rate.assert_called_once()
