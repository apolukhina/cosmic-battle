from interface import Interface, implements
from typing import List

from Commands.CommandException import CommandException


class Command(Interface):

    def execute(self) -> None:
        pass


class MacroCommand(implements(Command)):

    def __init__(self, commands: List[Command]):
        self.commands = commands

    def execute(self) -> None:
        try:
            for c in self.commands:
                c.execute()
        except Exception as e:
            raise CommandException(f'Something error while executing commands. Details: {e.args[0]}')

