from interface import implements

from Interfaces.interfaces import Rotatable

from Commands.Interface import Command, MacroCommand
from Commands.ChangeVelocityCommand import ChangeVelocity


class Rotate(implements(Command)):

    def __init__(self, rotatable: Rotatable) -> None:
        self.r = rotatable

    def execute(self) -> None:
        try:
            direction: int = self.r.get_direction()
            angular_velocity: int = self.r.get_angular_velocity()
            direction_numbers: int = self.r.get_directions_number()
            self.r.set_direction(direction=((direction + angular_velocity) % direction_numbers))
        except KeyError as e:
            raise KeyError(f'Such object cannot rotate. Details: {e.args[0]}')
        except AttributeError as e:
            raise AttributeError(f'Object attributes error: {e}')


class RotateAndChangeVelocity(MacroCommand):

    def __init__(self, rotate: Rotate, change_velocity: ChangeVelocity):
        super().__init__(commands=[rotate, change_velocity])
