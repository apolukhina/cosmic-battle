from interface import implements

from Interfaces.interfaces import WithFuel
from Commands.Interface import Command


class BurnFuel(implements(Command)):

    def __init__(self, with_fuel: WithFuel):
        self.with_fuel = with_fuel

    def execute(self):
        try:
            fuel: int = self.with_fuel.get_fuel()
            burn_rate: int = self.with_fuel.get_burn_rate()
            self.with_fuel.set_fuel(fuel=fuel - burn_rate)
        except KeyError as e:
            raise KeyError(f'Such object without fuel. Details: {e.args[0]}')
        except AttributeError as e:
            raise AttributeError(f'Object attributes error: {e}')
