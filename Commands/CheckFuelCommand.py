from interface import implements

from Interfaces.interfaces import WithFuel
from Commands.Interface import Command
from Commands.CommandException import CommandException


class CheckFuel(implements(Command)):

    def __init__(self, with_fuel: WithFuel) -> None:
        self.with_fuel = with_fuel

    def execute(self) -> None:
        try:
            fuel: int = self.with_fuel.get_fuel()
            burn_rate: int = self.with_fuel.get_burn_rate()
            if fuel - burn_rate < 0:
                raise CommandException('Not enough fuel to carry out the command.')
        except KeyError as e:
            raise KeyError(f'Such object without fuel. Details: {e.args[0]}')
