import math

from interface import implements

from Interfaces.interfaces import VelocityChangeable
from Models.Position import Position
from Commands.Interface import Command


class ChangeVelocity(implements(Command)):

    def __init__(self, velocity_changeable: VelocityChangeable) -> None:
        self.velocity_changeable = velocity_changeable

    def execute(self) -> None:
        try:
            velocity: Position = self.velocity_changeable.get_velocity()
            velocity_module: float = math.sqrt(pow(velocity.x, 2) + pow(velocity.y, 2))
            direction: int = self.velocity_changeable.get_direction()
            number_of_directions: int = self.velocity_changeable.get_directions_number()
            new_velocity = Position(x=math.ceil(velocity_module * math.cos(direction/number_of_directions * 360)),
                                    y=math.ceil(velocity_module * math.sin(direction/number_of_directions * 360)))
            self.velocity_changeable.set_velocity(velocity=new_velocity)
        except KeyError as e:
            raise KeyError(f'Such object is not velocity changeable. Details: {e.args[0]}')
        except AttributeError as e:
            raise AttributeError(f'Object attributes error: {e}')
