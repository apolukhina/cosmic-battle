from interface import implements
from typing import List

from Models.Position import Position
from Interfaces.interfaces import Movable

from Commands.Interface import Command, MacroCommand
from Commands.CheckFuelCommand import CheckFuel
from Commands.BurnFuelCommand import BurnFuel


class Move(implements(Command)):

    def __init__(self, movable: Movable) -> None:
        self.m = movable

    def execute(self):
        try:
            position: Position = self.m.get_position()
            velocity: Position = self.m.get_velocity()
            self.m.set_position(position=Position(x=position.x + velocity.x, y=position.y + velocity.y))
        except KeyError as e:
            raise KeyError(f'Such object cannot move. Details: {e.args[0]}')
        except AttributeError as e:
            raise AttributeError(f'Object attributes error: {e}')


class MoveWithFuel(MacroCommand):

    def __init__(self, check_fuel: CheckFuel, move: Move, burn_fuel: BurnFuel):
        super().__init__([check_fuel, move, burn_fuel])

