from interface import Interface
from Models.Position import Position


# Movable objects
class Movable(Interface):

    def get_position(self) -> Position:
        pass

    def get_velocity(self) -> Position:
        pass

    def set_position(self, position: Position):
        pass


# Rotatable objects
class Rotatable(Interface):

    def get_direction(self) -> int:
        pass

    def set_direction(self, direction: int):
        pass

    def get_angular_velocity(self) -> int:
        pass

    def get_directions_number(self) -> int:
        pass


# With fuel objects
class WithFuel(Interface):

    def get_fuel(self) -> int:
        pass

    def set_fuel(self, fuel: int) -> None:
        pass

    def get_burn_rate(self) -> int:
        pass


# Velocity changeable
class VelocityChangeable(Movable, Rotatable, Interface):

    def set_velocity(self, velocity: Position) -> None:
        pass
